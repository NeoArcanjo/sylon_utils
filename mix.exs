defmodule SylonUtils.MixProject do
  use Mix.Project

  @version "0.1.6"

  def project do
    [
      app: :sylon_utils,
      version: @version,
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: description(),
      package: package()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :httpoison, :jun],
      mod: {SylonUtils.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:mogrify, "~> 0.7.3"},
      {:jun, github: "NeoArcanjo/jun"},
      {:ex_doc, ">= 0.0.0", only: :dev, runtime: false},
      {:httpoison, "~> 1.6"},
      {:nimble_csv, "~> 0.6"}
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end

  defp description do
    "Coleção de funções para reutilizar em meus projetos pessoais"
  end

  defp package do
    [
      files: ["lib", "mix.exs", "README*", "CHANGELOG*", "LICENSE*"],
      maintainers: ["Rafael Arcanjo"],
      licenses: ["MIT"],
      links: %{"GitLab" => "https://gitlab.com/NeoArcanjo/"}
    ]
  end
end
