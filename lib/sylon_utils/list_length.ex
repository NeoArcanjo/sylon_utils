defmodule SylonUtils.List do
  @spec lenght([any]) :: non_neg_integer
  def lenght([]), do: 0
  def lenght([_ | tail]), do: 1 + lenght(tail)
end
