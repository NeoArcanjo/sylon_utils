defmodule SylonUtils.ScanFile do
  @moduledoc """
  Modulo para mapear o tratamento de linhas de um arquivo
  """

  @spec readlines(atom | pid, any) :: nil
  def readlines(file, function) do
    case IO.read(file, :line) do
      :eof ->
        nil

      line ->
        function.(line)

        readlines(file, function)
    end
  end

  # NimbleCSV.define(MyParser, separator: "\t", escape: "\"")

  # def csv(filename \\ "priv/contacts (1).csv") do
  #   filename
  #   |> File.open()
  #   |> IO.stream(:all)
  #   |> MyParser.parse_stream()
  #   |> Stream.map(fn line ->
  #     IO.stream(line, :line)
  #   end)
  # end
end
