defmodule SylonUtils.IconeProfile do
  @moduledoc """
  Documentation for IconeProfile.
  """

  alias SylonUtils.Imagem

  def main(input) do
    input
    |> hash_input
    |> create_color
    |> table_build
    |> rem_odd
    |> pixel_build
    |> draw(input)

    # |> save(input)
  end

  def save(image, input) do
    File.write("priv/static/images/#{input}.png", image)
  end

  def hex_color(color) do
    color
    |> Tuple.to_list()
    |> Enum.reduce("#", fn x, acc ->
      hex = Integer.to_string(x, 16)

      case String.length(hex) > 1 do
        true -> acc <> hex
        false -> acc <> "0" <> hex
      end
    end)
    |> String.downcase()
  end

  def draw(%Imagem{color: color, pixel_map: pixel_map}, input) do
    color = hex_color(color)

    import Mogrify
    alias SylonUtils.MogrifyDraw

    image =
      %Mogrify.Image{path: "priv/static/images/#{input}.png", ext: "png"}
      |> custom("size", "250x250")
      |> canvas("white")

    image =
      Enum.reduce(pixel_map, image, fn {{start1, start2}, {stop1, stop2}}, acc ->
        acc
        |> custom("fill", color)
        |> MogrifyDraw.rectangle(start1, start2, stop1, stop2)
      end)

    image
    |> create(path: "priv/static/images/")
  end

  def pixel_build(%Imagem{grid: grid} = imagem) do
    pixel_map =
      Enum.map(grid, fn {_valor, indice} ->
        h = rem(indice, 5) * 50
        v = div(indice, 5) * 50
        top_left = {h, v}
        bottom_right = {h + 50, v + 50}
        {top_left, bottom_right}
      end)

    %Imagem{imagem | pixel_map: pixel_map}
  end

  def create_color(%Imagem{hex: [r, g, b | _tail]} = imagem) do
    %Imagem{imagem | color: {r, g, b}}
  end

  def table_build(%Imagem{hex: hex} = imagem) do
    grid =
      hex
      |> Enum.chunk_every(3, 3, :discard)
      |> Enum.map(&mirror/1)
      |> List.flatten()
      |> Enum.with_index()

    %Imagem{imagem | grid: grid}
  end

  def mirror([fst, sec | _tail] = row) do
    row ++ [sec, fst]
  end

  def rem_odd(%Imagem{grid: grid} = imagem) do
    grid = Enum.filter(grid, fn {valor, _indice} -> rem(valor, 2) == 0 end)
    %Imagem{imagem | grid: grid}
  end

  def hash_input(input) do
    hex =
      :crypto.hash(:md5, input)
      |> :binary.bin_to_list()

    %Imagem{hex: hex}
  end
end
